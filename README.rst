=====
Texas Advanced Computing Center System Monitor
=====

System Monitor provides current status of TACC compute and storage resources.


Quick start
-----------

1. Add a reference to sysmon in Django's requirements.txt, use appropriate commit as needed::

    -e git+https://bitbucket.org/rochaa/tacc-sysmon.git@288a8f0f917e04f9dee68cde97e4991c9d9ac87d#egg=sysmon

2. Add "SYSMON_URL" property and value to settings::

    SYSMON_URL = 'https://portal.tacc.utexas.edu/commnq/index.json'

3. Add "sysmon" to your INSTALLED_APPS setting::

    INSTALLED_APPS = [
        ...
        'sysmon',
    ]

4. Include the sysmon URLconf in your Django project's urls.py like this::

    url(r'^sysmon/', include('sysmon.urls')),

5. Run `python manage.py migrate sysmon` to create the sysmon models.

6. Visit http://127.0.0.1:8000/sysmon/
