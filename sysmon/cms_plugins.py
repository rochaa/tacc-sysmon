from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from sysmon import views
from sysmon.models import SingleSystemPluginModel, MultiSystemPluginModel
import logging

logger = logging.getLogger(__name__)

@plugin_pool.register_plugin
class SingleSystemPlugin(CMSPluginBase):
    model = SingleSystemPluginModel
    name = _("Single System Monitor")
    render_template = "sysmon/basic_display.html"
    cache = False
    allow_children = True
    text_enabled = True

    def render(self, context, instance, placeholder):
        context = super(SingleSystemPlugin, self).render(context, instance, placeholder)
        system_statuses = []
        try:
            system_statuses = views.get_sysmon_data(\
                requested_systems=instance.systems)
        except Exception as exc:
            logger.error('Error retrieving sysmon data: {0}'.format(exc))
        context['statuses'] = system_statuses
        return context

@plugin_pool.register_plugin
class MultiSystemPlugin(CMSPluginBase):
    model = MultiSystemPluginModel
    name = _("Multi-System Monitor")
    render_template = "sysmon/multi_display.html"
    cache = False
    allow_children = True
    text_enabled = True

    def render(self, context, instance, placeholder):
        context = super(MultiSystemPlugin, self).render(context, instance, placeholder)
        system_statuses = []
        try:
            system_statuses = views.get_sysmon_data(\
                requested_systems=instance.systems)
        except Exception as exc:
            logger.error('Error retrieving sysmon data: {0}'.format(exc))
        context['statuses'] = system_statuses
        return context